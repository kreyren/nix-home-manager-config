{ config, pkgs, ... }: {
	# NOTE(Krey): Just so there is something here
	programs.vim.enable = true;

	# FIXME(Krey): Blocked by https://github.com/nix-community/home-manager/issues/2152
	#programs.freetube.enable = true;
}