{ pkgs, ... }: {
	programs.firefox = {
		profiles = {
			myprofile = {
				settings = {
					"general.smoothScroll" = false;
					# Tor integration
					## FIXME(Krey): Apply this only if tor is present
					"network.proxy.socks" = "127.0.0.1";
					"network.proxy.socks_port" = "9050";
					"network.proxy.socks_remote_dns" = true;
				};
			};
		};
		# NOTE(Krey): List of exensions https://nur.nix-community.org/repos/rycee/
		extensions = with pkgs.nur.repos.rycee.firefox-addons; [
			https-everywhere
			privacy-badger
			clearurls
			keepassxc-browser
			sponsorblock
			decentraleyes
			ublock-origin
			darkreader
			videospeed
			privacy-redirect
		];
}