# NixOS shell configuration to bootstrap the required dependencies
# NOTE(Krey): Uses mozilla's overlay, because nix's upstream is slow

{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = [ pkgs.buildPackages.cargo-make ];
}