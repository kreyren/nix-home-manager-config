{ config, ... }: {
	# Git integration
	programs.git.signing.signByDefault = config.programs.git.enable;
}