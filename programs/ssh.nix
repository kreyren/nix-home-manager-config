{ services, ... }: {
	programs.ssh = {
		proxyCommand = if(services.tor.torsocks.enable == true)
			then "torsocks"
			else null;
	};
}