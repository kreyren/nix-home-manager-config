{ ... }: {
	programs.git = {
		userName = "Jacob Hrbek";
		userEmail = "kreyren@fsfe.org";
		# FIXME(Krey): Use tor for proxy if it's set up on the system
	};

	# NOTE(Krey): Fix for `No pinentry` error
	programs.gnupg.agent = {
		enableSSHSupport = true;
		pinentryFlavor = "curses";
	}
}