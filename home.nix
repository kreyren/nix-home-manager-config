### Initialization file used to deploy network-wide configuration

# Reference by real-life examples:
# - https://github.com/balsoft/nixos-config
# - https://github.com/MatthewCroughan/nixcfg

{ config, lib, ... }:
let
	# Global variables
	## NOTE(Krey): Uses `lib.splitString` to remove trailing new-line
	## FIXME(Krey): Doesn't work
	#hostname = lib.head (lib.splitString "\n" (builtins.readFile /etc/hostname));
	#hostname = "leonid";
	#domain = "domain";
	# NOTE(Krey): Decide what to do with this
	#homeNixDir = "$HOME/.local/share/home-manager";

	# NOTE(Krey): Handling of https://github.com/NixOS/nixpkgs/issues/128286
	#nixFilesIn = path:
	#	let
	#		names = lib.filter (lib.hasSuffix ".nix") (lib.attrNames (builtins.readDir path));
	#	in
	#		map (x: path + "/${x}") names;
in {
	#networking.hostName = hostname;
	#networking.domain = domain;
	
	imports = [
		./machines/leonid.nix
	#	(/. + "/machines/" + "${config.networking.hostName}" + ".nix")
	]; 
	#++ nixFilesIn ./programs;

	# Let Home Manager install and manage itself.
	programs.home-manager.enable = true;

	# Home Manager needs a bit of information about you and the
	# paths it should manage.
	home.username = "kreyren";
	home.homeDirectory = "/home/kreyren";

	home.stateVersion = "21.05";

	# Global packages
	programs.bash.enable = true;
	programs.vim.enable = true;
	programs.alacritty.enable = true;
	programs.firefox.enable = true;
	programs.gpg.enable = true;
	programs.gnupg.enable = true;
	programs.git.enable = true;
	programs.htop.enable = true;
}
